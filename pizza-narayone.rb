require './base.rb'

class Tempura < Parser
  HOST = "http://pizza-narayone.ru"
  def self.main
    init
    fl = check_cat_id "pizza-narayone"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "pizza-narayone"
    @tovar_index = @fl_o.values.max if @fl_o


    a = get("http://pizza-narayone.ru/css/PizzaCss.css").scan(/(#.*) {\s+background-image\: url\(\.\.(.*)\)/).to_h
    b = get("http://pizza-narayone.ru/css/BeveragesCss.css").scan(/(#.*) {\s+background-image\: url\(\.\.(.*)\)/).to_h
    c = get("http://pizza-narayone.ru/css/AllRestCss.css").scan(/(#.*) {\s+background-image\: url\(\.\.(.*)\)/).to_h
    @hash_css = (a.merge b).merge c
    # return ""
    %w(/~Пицца /sushi.php~Суши /pies.php~Пироги /zakuski.php~Закуски /beverages.php~Напитки).each do |cat_line|
      cat_link,cat_name = cat_line.split("~")
      html = get(HOST+cat_link)
      doc = Nokogiri::HTML(html)
      parrent_cat = gen_id(fl,"0",cat_name)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat_name}</category>\n" 
      doc.xpath("//div[starts-with(@class,'Item1')]").each do |tov|
        get_offers tov, parrent_cat
      end
    end
    # puts @cats
    gen_xml(HOST,"pizza-narayone","pizza-narayone")
  end

  def self.get_offers(tovar,cat_id)

      name = tovar.xpath("./div[starts-with(@id,'PName')]").text.strip
      return if name == ""
      description = tovar.css("div#PIngredients").text.strip
      image = @hash_css["##{tovar["id"]}"] rescue ""
      price = tovar.css("div#PriceP span").text.to_price
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{price}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST}#{image}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main