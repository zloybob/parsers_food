require './base.rb'

class Tempura < Parser
  HOST = "http://kentasushi.ru"
  def self.main
    init
    fl = check_cat_id "kentasushi"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "kentasushi"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    cc = doc.css("ul.kategor").xpath("./li/a")
    cc.pop
    cc.each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text.strip)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text.strip}</category>\n" 
      Nokogiri::HTML(get HOST+cat["href"]).css("div.tovar2").each do |l|
        get_offers l, parrent_cat
      end
    end
   # puts @cats
  gen_xml(HOST,"kentasushi","kentasushi")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("h2 a").first.text
      description = tovar.css("div.t_note").text.strip
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("li.price").text.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("a.highslide").first['href'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main