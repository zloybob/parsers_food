require './base.rb'

class Fsfood < Parser
  HOST = "http://www.fsfood.ru"
  def self.main
    init
    ids = {}
    html = get(HOST+"/?route=product/category&path=all")
    doc = Nokogiri::HTML(html)
    fl = check_cat_id "fsfood"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "fsfood"
    @tovar_index = @fl_o.values.max if @fl_o
    parrent_cat = 0
    doc.css("div#menu ul li").each do |cat|
      if cat.css("div ul").size == 0
        cat_id = gen_id(fl,parrent_cat,cat.css("a").text)
        @cats << "<category id='#{cat_id}' parentId='#{parrent_cat}'>#{cat.css("a").text }</category>\n" 
      else
        parrent_cat = gen_id(fl,"0",cat.css("a").first.text)
        @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.css("a").first.text}</category>\n" 
      end
    end
    doc.css("input.button1").map{|a| a["onclick"].gsub("addQtyToCart('","").gsub("', '","~~").gsub("');","")}.each{|a| id,id2=a.split("~~");ids[id]=id2}
    ids.each do |id, cat_id|
      html = get "http://www.fsfood.ru/?route=product/product&product_id=#{id}"
      doc = Nokogiri::HTML(html)
      get_offers doc, cat_id
    end
    gen_xml(HOST,"Fsfood","fsfood")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.description h1").text
      description = tovar.css("div.prod-text-top").text

      t_i = gen_offer_id @fl_o,name,description

      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.price").first.text.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{tovar.css("a.jqzoom").first['href']}</picture>
      </offer>\n"
    
  end

  
end

Fsfood.main