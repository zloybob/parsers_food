require './base.rb'

class Tempura < Parser
  HOST = "http://sooshiroom.com"
  def self.main
    init
    fl = check_cat_id "sooshiroom"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "sooshiroom"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("ul.menu_vert li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get HOST+cat["href"]
      tovar = Nokogiri::HTML(html)
      tovar.css("div.tovar_item").each do |tov|
        html3 = get HOST+"/"+tov.css("div.tovar_item_img a").first['href']
        tov2 = Nokogiri::HTML(html3)
        get_offers(tov2,parrent_cat)
      end
      sub_url = tovar.css("a.ditto_next_link").first['href'] rescue nil
      while sub_url
        html2 = get HOST+sub_url
        tovar2 = Nokogiri::HTML(html2)
        tovar2.css('div.tovar_item').each do |tov|
          html3 = get HOST+"/"+tov.css("div.tovar_item_img a").first['href']
          tov2 = Nokogiri::HTML(html3)
          get_offers(tov2,parrent_cat)
        end
        sub_url = tovar2.css("a.ditto_next_link").first['href'] rescue nil
      end


    end

    puts @cats
   
    gen_xml(HOST,"SooshiRoom","sooshiroom")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.liders_title h2").text
      description = tovar.css("div#content_text p").map{|a| a.text}.join(" ")
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("span.shk-price").text.strip}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+"/"+tovar.css("div#tovar_img img").first['src'] rescue ""}</picture>
      </offer>\n"
    
  end
  
end

Tempura.main