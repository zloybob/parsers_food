require './base.rb'

class Tempura < Parser
  HOST = "http://cosmoroll.su"
  def self.main
    init
    fl = check_cat_id "cosmoroll"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "cosmoroll"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST+"/menu/")
    @titles = []
    @o_hash = {}
    doc = Nokogiri::HTML(html)
    doc.css("ul#allaround_menu li").each_with_index do |cat,i|
      next if [0,1].include? i
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text.gsub("&","&amp;")}</category>\n" 
      # next if "http://cosmoroll.su/cat/soup" != cat.css("a").first['href']
      html = open cat.css("a").first['href']
      parts = html.read.split('<div class="headline_inner_wrapper">')
      
      parts.shift
      puts parts.size
      parts.each do |part|
        doc2 = Nokogiri::HTML(part)
        title = doc2.css("h2.main_header").text
        unless cat.text == title
          sub_cat = gen_id(fl,parrent_cat,title)
          @cats << "<category id='#{sub_cat}' parentId='#{parrent_cat}'>#{title.gsub("&","&amp;")}</category>\n" 
        else
          sub_cat = parrent_cat
        end
        doc2.css("div.text_wrapper h3 a").each do|a|
          next if @titles.include? a['href']
          @titles << a['href'] 
          html2 = get a['href']
          doc3 = Nokogiri::HTML(html2)
          get_offers doc3, sub_cat
        end
      end
    end

    puts @cats
   
    gen_xml(HOST,"Косморолл","cosmoroll")
  end

  def self.get_offers(tovar,cat_id)
    name = tovar.css("div.text_wrapper h3").text
    description = tovar.css("span.sub_header p").map{|a| a.text}.join(" ")
    price = if tovar.css("p.price").first.css('del').size > 0
      tovar.css("p.price").first.css("ins").first.text.scan(/\d+/).join
    else
      tovar.css("span.amount").first.text.scan(/\d+/).join
    end
    img = tovar.css("img.content_image").first['src'] rescue ""
    if tovar.css("form.variations_form").size > 0
      tovar.css("select#pa_mak option").each_with_index do |opt,j|
        next if j == 0
        @o_hash[opt['value']] = opt.text
      end
      JSON.parse(tovar.css("form.variations_form").first['data-product_variations']).each do |hh|
        name2 = Unicode::capitalize @o_hash[hh['attributes']['attribute_pa_mak']]
        price2 = hh['price_html'].scan(/\d+/).join
        price = price2 if price2.to_i > 0
        t_i = gen_offer_id @fl_o,name2,description
        @offers << "<offer id=\"#{t_i}\">
        <url>#{}</url>
        <price>#{price}</price>
        <currencyId>RUR</currencyId>
        <categoryId>#{cat_id}</categoryId>
        <name>#{name2.gsub("&","&amp;")}</name>
        <description>#{description.gsub("&","&amp;")}</description>
        <picture>#{img}</picture>
        </offer>\n"
      end
    else
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{price}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name.gsub("&","&amp;")}</name>
      <description>#{description.gsub("&","&amp;")}</description>
      <picture>#{img}</picture>
      </offer>\n"
    end
  end
  
end

Tempura.main