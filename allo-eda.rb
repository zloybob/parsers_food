require './base.rb'

class Tempura < Parser
  HOST = "http://allo-eda.ru"
  def self.main
    init
    fl = check_cat_id "allo-eda"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "allo-eda"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div#left_menu").xpath("./div").each do |cat|
    	next if cat.css("ul").size == 0
    	cat_name = cat.css("div.main_name").text.strip.gsub(/\s+/," ")
      parrent_cat = gen_id(fl,"0",cat_name)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat_name}</category>\n" 
      sub_cat = cat.css("ul li a")
      if sub_cat.size > 1
      	sub_cat.each do |sub|
      		sub_parrent_cat = gen_id(fl,parrent_cat,sub.text)
      		@cats << "<category id='#{sub_parrent_cat}' parentId='#{parrent_cat}'>#{sub.text}</category>\n" 
    		 	Nokogiri::HTML(get HOST+sub["href"]).css("div.one_product").each do |tov|
        		get_offers tov, sub_parrent_cat
      		end
      	end
      else
      	Nokogiri::HTML(get HOST+cat.css("a").first["href"]).css("div.one_product").each do |tov|
        	get_offers tov, parrent_cat
      	end
      end
    end
    gen_xml(HOST,"allo-eda","allo-eda")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.name").text
      description = Nokogiri::HTML(tovar.css("div.increase").first["data-about"]).text.gsub(/\s+/," ")
      t_i = gen_offer_id @fl_o,name,description
      img = tovar.css("div.increase").first["data-main-img"] rescue ""
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.price").text.to_price}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+img}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main