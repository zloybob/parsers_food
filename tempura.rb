require './base.rb'

class Tempura < Parser
  HOST = "http://tempura.ru"
  def self.main
    init
    fl = check_cat_id "tempura"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "tempura"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST+"/shop/pizza.html")
    doc = Nokogiri::HTML(html)
    doc.css("td.infoBoxHeading a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get cat["href"]+"?on_page=100"
      tovar = Nokogiri::HTML(html)
      tovar.css("td.contentBoxContents1 tr.contentBoxContents1 td.contentBoxContents1").each do |tov|
        get_offers tov, parrent_cat
      end
    end
   
    gen_xml(HOST,"Темпура","tempura")
  end

  def self.get_offers(tovar,cat_id)
      title = tovar.css("b.link_r").text
      name = title
      description = tovar.css("div.link").text.gsub(title,"")
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.cena").first.text.gsub(" руб.","").strip}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{title}</name>
      <description>#{description}</description>
      <picture>#{HOST+"/shop/"+tovar.css("a.highslide").first['href'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main