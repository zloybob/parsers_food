require './base.rb'

class Porcia < Parser
  HOST = "http://pro-porcia.ru"
  def self.main
    init
    fl = check_cat_id "pro-porcia"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "pro-porcia"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div.root_category ul li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get HOST+"/"+cat["href"]
      tovar = Nokogiri::HTML(html)
      tovar.css("form.product_brief_block").each do |tov|
         get_offers tov, parrent_cat
      end
    end
     gen_xml(HOST,"PRO.PORCIA","pro-porcia")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.prdbrief_name").text
      description = tovar.css("div.prdbrief_brief_description p").map{|a| a.text}.join(" ").strip.gsub("\n"," ")
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("span.totalPrice").text.gsub(" руб.","")}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name.gsub("&","&amp;")}</name>
      <description>#{description.gsub("&","&amp;")}</description>
      <picture>#{HOST+tovar.css("td.img_prod img").first['src'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Porcia.main