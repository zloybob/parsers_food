require './base.rb'

class Tempura < Parser
  
  def self.main(from)
    s = "#{from}."
    s = ""  if from == "chlb"
      
    host = "http://#{s}nasushi.ru"
    init
    @offer_hash = {}
    fl = check_cat_id "nasushi_#{from}"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "nasushi_#{from}"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(host+"/eshop/")
    doc = Nokogiri::HTML(html)
    doc.css("table.categories-list a").each do |cat|
    	cat_name = cat.text
      next if cat_name == "Шашлык"
      parrent_cat = gen_id(fl,"0",cat_name)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat_name}</category>\n" 

      doc = Nokogiri::HTML(get(host+cat["href"]+"?all=1"))

      rolls = doc.css("div.roll-filter")
      if rolls.size == 0
        doc.css("div.product-item").each do |t|
          get_offers t,parrent_cat,host
        end
      else
        rolls.css("a").each do |roll|
          sub_cat = gen_id(fl,"0",roll.text)
          @cats << "<category id='#{sub_cat}' parentId='#{parrent_cat}'>#{roll.text}</category>\n" 
          doc2 = Nokogiri::HTML(get(host+cat["href"]+roll["href"]+"&all=1"))
          doc2.css("div.product-item").each do |t|
            get_offers t,sub_cat,host
          end
        end

      end
    end
    # puts @cats
    @offer_hash.each do |k,v|
      gen_offer v
    end
    gen_xml(host,"nasushi_#{from}","nasushi_#{from}")
  end

  def self.gen_offer(h)
    @offers << "<offer id=\"#{h['index']}\">
        <url>#{}</url>
        <price>#{h['price']}</price>
        <currencyId>RUR</currencyId>
        #{h['cat_id'].map{|c| "<categoryId>#{c}</categoryId>"}.join("\n")}
        <name>#{h['name']}</name>
        <description>#{h['description']}</description>
        <picture>#{h['img']}</picture>
        </offer>\n"
  end

  def self.get_offers(tovar,cat_id,host)
      name = tovar.css("div.product-name > text()").text
      description = tovar.css("div.product-announce span.more-info > text()").text
      r=Digest::MD5.hexdigest(name+description)
      unless @offer_hash[r]
        @offer_hash[r] ||= {}
        @offer_hash[r]["name"] = name
        @offer_hash[r]["description"] = description
        @offer_hash[r]["index"] = gen_offer_id @fl_o,name,description
        @offer_hash[r]["img"] = host+(tovar.css("div.product-photo a").first["href"] rescue "")
        @offer_hash[r]["price"] = (tovar.css("span.new-price").size > 0 ? tovar.css("span.new-price").first.text : tovar.css("div.product-price > text()").text).to_price
        @offer_hash[r]["cat_id"] ||= []
        @offer_hash[r]["cat_id"] << cat_id
      else
        @offer_hash[r]["cat_id"] << cat_id
      end
    
  end

  
end
%w(irk smr chlb).each do |item|
Tempura.main item
end