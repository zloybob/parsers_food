require './base.rb'

class Tempura < Parser
  HOST = "http://ko-sushka.ru"
  def self.main
    init
    fl = check_cat_id "ko-sushka"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "ko-sushka"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("ul.eshop-cat-tree_level_1").xpath("./li/a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text.strip)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text.strip}</category>\n" 
      Nokogiri::HTML(get(HOST+cat["href"]).force_encoding("utf-8").split("Приправы").first).css("div.eshop-item-small__content").each do |l|
        get_offers l, parrent_cat
      end
    end
   # puts @cats
  gen_xml(HOST,"ko-sushka","ko-sushka")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.eshop-item-small__header a").first.text
      description = tovar.css("div.eshop-item-small__spec-announce").text
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.eshop-item-small__price-actual").text.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{tovar.css("img.eshop-item-small__img").first['src'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main