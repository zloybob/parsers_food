require './base.rb'

class Hit < Parser
  HOST = "http://hit72.com"
  def self.main
    init
    fl = check_cat_id "hit72"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "hit72"
    @tovar_index = @fl_o.values.max if @fl_o
    parrent_cat = 0
    html = get(HOST)
    doc = Nokogiri::HTML(html.encode("UTF-8","UTF-8"))
    doc.css("div.sidebar ul li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get HOST+cat["href"]+"?page=showall"
      tovar = Nokogiri::HTML(html)
      tovar.css("div.item").each do |tov|
         get_offers tov, parrent_cat
      end
    end
     gen_xml(HOST,"Хит72","hit72")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.head").text
      description = tovar.css("div.comment p").text.strip.gsub("\n","")
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.price p.pr").text.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("div.headimg a").first['href'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Hit.main