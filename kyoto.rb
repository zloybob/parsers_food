require './base.rb'

class Kyoto < Parser
  HOST = "http://kyoto-sushi.ru"
  def self.main
    init
    fl = check_cat_id "kyoto"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "kyoto"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST+"/magazin")
    doc = Nokogiri::HTML(html)
    doc.css("ul#lmenu").xpath("./li").each do |li|
      main_cat = Unicode::capitalize li.css('a.c span').text
      next if main_cat == ""
      parrent_cat = gen_id(fl,"0",main_cat)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{main_cat}</category>\n" 
      sub_cat = li.css('ul li.d')
      if sub_cat.size > 0
        sub_cat.each do |sub|
          sub_cat = Unicode::capitalize sub.css('a.d span').text
          cat_id = gen_id(fl,parrent_cat,sub_cat)
          @cats << "<category id='#{cat_id}' parentId='#{parrent_cat}'>#{sub_cat}</category>\n" 
          html = get(HOST+sub.css("a.d").first['href'])
          off = Nokogiri::HTML(html)
          get_offers off,cat_id
        end
      else
        html = get(HOST+li.css("a.c").first['href'])
        off = Nokogiri::HTML(html)
        get_offers(off,parrent_cat)
      end
    end
    gen_xml(HOST,"Киото","kyoto")
  end


  def self.get_offers(tovar,cat_id)     
    tovar.css("div.tovar2").each do |tov|
          name = tov.css("h2 a").text.gsub(/\(\d+ .*\)$/,"")
          description = tov.css("div.t_note").text.strip
          t_i = gen_offer_id @fl_o,name,description
          @offers << "<offer id=\"#{t_i}\">
          <url>#{}</url>
          <price>#{tov.css("li.price span b").text.scan(/\d+/).join}</price>
          <currencyId>RUR</currencyId>
          <categoryId>#{cat_id}</categoryId>
          <name>#{name}</name>
          <description>#{description}</description>
          <picture>#{HOST+tov.css("div.wrap a.highslide img").first["style"].gsub(/.*url\((.*)\).*/,"\\1")}</picture>
          </offer>\n"
    end
  end

  
end

Kyoto.main