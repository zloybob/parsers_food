require './base.rb'

class Segun < Parser
  HOST = "http://segun58.ru"
  def self.main
    init
    fl = check_cat_id "segun58"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "segun58"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("a.l1").each_with_index do |cat,i|
      next if i == 0
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get(HOST+cat["href"])
      tovar = Nokogiri::HTML(html)
      sub = tovar.css("a.l2")
      if sub.size > 0
        sub.each do |sub_cat|
          sub_cat_index =  gen_id(fl,parrent_cat,sub_cat.text)
          @cats << "<category id='#{sub_cat_index}' parentId='#{parrent_cat}'>#{sub_cat.text}</category>\n" 

          Nokogiri::HTML(get(HOST+sub_cat["href"])).css("div.tovar2").each do |tov|
            get_offers tov, sub_cat_index
          end
        end
      else
        tovar.css("div.tovar2").each do |tov|
          get_offers tov, parrent_cat
        end
      end
    end
  
    gen_xml(HOST,"Сёгун","segun58")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("p.h2 a").text
      description = tovar.css("div.t_note").text
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("li.price span b").first.text}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("a.highslide").first['href'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Segun.main