require './base.rb'

class Tempura < Parser
  HOST = "http://yaponika-sushi.ru/"
  def self.main
    init
    fl = check_cat_id "yaponika-sushi"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "yaponika-sushi"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div.cats4").xpath("./ul").xpath("./li").each do |cat|
      cat_name = cat.css("div.R a").first.text
      parrent_cat = gen_id(fl,"0",cat_name)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat_name}</category>\n" 
      sub_cat = cat.css("ul li a")
      if sub_cat.size > 0
        sub_cat.each do |sub|
          sub_parrent_cat = gen_id(fl,parrent_cat,sub.text)
          @cats << "<category id='#{sub_parrent_cat}' parentId='#{parrent_cat}'>#{sub.text}</category>\n" 
          Nokogiri::HTML(get(HOST+sub['href'])).css("div.catalog div.item").each do |tov|
            get_offers tov, sub_parrent_cat
          end
        end
      else
        Nokogiri::HTML(get(HOST+cat.css("div.R a").first['href'])).css("div.catalog div.item").each do |tov|
          get_offers tov, parrent_cat
        end
      end
    end
   # puts @cats
    gen_xml(HOST,"yaponika-sushi","yaponika-sushi")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("h3").text.strip
      puts name
      description = tovar.css("div.brief").text.strip
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.price").first.text.to_price}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("div.img a img").first['src'] rescue ""}</picture>
      </offer>\n"  
  end
end

Tempura.main