require './base.rb'

class Tempura < Parser
  HOST = "http://alex-pizza.ru"
  def self.main
    init
    fl = check_cat_id "alex-pizza"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "alex-pizza"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div#menucats ul li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get HOST+cat["href"]
      tovar = Nokogiri::HTML(html)
      tovar.css("div.item div.c").each do |tov|
        get_offers tov, parrent_cat
      end
    end
    # puts @cats
    gen_xml(HOST,"alex-pizza","alex-pizza")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("h2").text.gsub("&","&amp;")
      description = tovar.xpath("./p").text.gsub("&","&amp;")
      sub = tovar.css("div.check")
      sub.each do |p|
        
        price = p.css("em").text.to_price
        disc_plus = p.text.strip.split("   ").first.gsub("  "," ").gsub("&","&amp;")
        n = sub.size > 1 ? name+disc_plus : name
        t_i = gen_offer_id @fl_o,n,description
        @offers << "<offer id=\"#{t_i}\">
        <url>#{}</url>
        <price>#{price}</price>
        <currencyId>RUR</currencyId>
        <categoryId>#{cat_id}</categoryId>
        <name>#{n}</name>
        <description>#{description}</description>
        <picture>#{HOST+tovar.css("a img").first['src'] rescue ""}</picture>
        </offer>\n"
      end
    
  end

  
end

Tempura.main