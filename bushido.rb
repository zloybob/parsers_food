require './base.rb'

class Bushido < Parser
  HOST = "http://bushido-nn.ru"
  def self.main
    init
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    fl = check_cat_id "bushido"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "bushido"
    @tovar_index = @fl_o.values.max if @fl_o
    doc.css("div.moduletable_leftmenu div.jshop_menu_level_0 a").each do |cat|
      parrent_cat = if fl
        n = Digest::MD5.hexdigest("0"+cat.text.strip)
        fl[n] ? fl[n] : (@cat_index+=1)
      else
        @cat_index+=1
      end
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text.strip}</category>\n" 
      @md5_cat_str << "#{Digest::MD5.hexdigest("0"+cat.text.strip)};#{parrent_cat}\n"
      html = open HOST+cat["href"]
      tovar = Nokogiri::HTML(html)
      tovar.css('table.product').each do |tov|
        get_offers(tov,parrent_cat)
      end
      t = tovar.css("ul.pagination")
      sub_url = t.xpath("//a[starts-with(@title,'Следующая')]").first['href']  rescue nil
      while sub_url
        html2 = open HOST+sub_url
        tovar = Nokogiri::HTML(html2)
        tovar.css('table.product').each do |tov|
          get_offers(tov,parrent_cat)
        end
        t = tovar.css("ul.pagination")
        sub_url = t.xpath("//a[starts-with(@title,'Следующая')]").first['href']  rescue nil
      end
    end
    gen_xml(HOST,"Бушидо","bushido")
  end


  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.name a").text.strip
      description = tovar.css("div.description").text.strip
      t_i = if @fl_o
        n = Digest::MD5.hexdigest(name+description)
        @fl_o[n] ? @fl_o[n] : (@tovar_index+=1)
      else
        @tovar_index+=1
      end
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.jshop_price_abs").text.gsub("р.","").strip}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{tovar.css("td.image a img.jshop_img").first['src'] rescue ""}</picture>
      </offer>\n"
      @md5_offer_str << "#{Digest::MD5.hexdigest(name+description)};#{t_i}\n"
    
  end

  
end

Bushido.main