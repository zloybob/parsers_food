require './base.rb'

class Tempura < Parser
  HOST = "http://limepizza.ru"
  def self.main
    init
    fl = check_cat_id "limepizza"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "limepizza"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("ul.categories li a").each do |cat|
      cat_text=cat.css("span div").text.strip
      parrent_cat = gen_id(fl,"0",cat_text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat_text}</category>\n" 
      html = get HOST+cat["href"]
      tovar = Nokogiri::HTML(html)
      tovar.css("ul.items").xpath("./li").each do |tov|
        get_offers tov, parrent_cat
      end
    end
    # puts @cats
    gen_xml(HOST,"limepizza","limepizza")
  end

  def self.get_offers(tovar,cat_id)

    image = tovar.css("div.item-picture a").first["href"] rescue ""
    name = tovar.css("div.item-short-info div").text.strip.gsub("&","&amp;")
    description = tovar.css("div.item-short-info p").text.strip.gsub("&","&amp;").gsub(/\s/," ")

      sub = tovar.css("div.group1")
      if sub.size > 0
        sub.each do |p| 
          price = p.css("div.proce").text.to_price
          disc_plus = p.css("div.optional-description").text.strip.gsub("&","&amp;")
          n = name+" "+disc_plus
          t_i = gen_offer_id @fl_o,n,description
          @offers << "<offer id=\"#{t_i}\">
          <url>#{}</url>
          <price>#{price}</price>
          <currencyId>RUR</currencyId>
          <categoryId>#{cat_id}</categoryId>
          <name>#{n}</name>
          <description>#{description}</description>
          <picture>#{HOST+image}</picture>
          </offer>\n"
        end
      else
          t_i = gen_offer_id @fl_o,name,description
          price = tovar.css("div.proce").text.to_price
          @offers << "<offer id=\"#{t_i}\">
          <url>#{}</url>
          <price>#{price}</price>
          <currencyId>RUR</currencyId>
          <categoryId>#{cat_id}</categoryId>
          <name>#{name}</name>
          <description>#{description}</description>
          <picture>#{HOST+image}</picture>
          </offer>\n"
      end
    
  end

  
end

Tempura.main