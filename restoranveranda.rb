require './base.rb'

class Restoranveranda < Parser
  HOST = "http://restoranveranda.ru"
  def self.main
    init
    fl = check_cat_id "restoranveranda"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "restoranveranda"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST+"/menu.html").force_encoding("windows-1251")


    ar = html.split('<td colspan="4" align="center"><img src="files/razdelitel.png" width="800" height="24" /></td>')
    ar.pop
    ar.each_with_index do |cat,i|
      cat = cat.split('<table width="850" border="0" >').last if i == 0 
      doc = Nokogiri::HTML(cat)
      parrent_cat = gen_id(fl,"0",doc.css("tr td p strong").text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{doc.css("tr td p strong").text}</category>\n" 

      doc.css("tr").each do |tr|
        next if tr.css("td").size < 4
        tds = tr.xpath("./td[starts-with(@valign,'top')]")
        next if tds.size < 3
        name = if tds.first.css("strong").size > 0
          tds.first.css("strong > text()").first.text.strip
        else
          tds.first.text.split("\n").first
        end
        next if "СТЕЙКИ" == name || "КИСЛОМОЛОЧНЫЕ ПРОДУКТЫ" == name
        description = "#{tds.css("span.opisaniye").text.strip} #{tds[1].text.strip}"
        price = tds.last.text.gsub(" руб.","").strip
        img = tr.css("td").first.css("img").first["src"] rescue ""
        get_offers(name,description,price,img,parrent_cat)
      end
      
    end
    gen_xml(HOST,"ВЕРАНДА","restoranveranda")
  end

  def self.get_offers(name,description,price,img,cat_id)
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{price}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+"/"+img}</picture>
      </offer>\n"
    
  end

  
end

Restoranveranda.main