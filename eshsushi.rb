require './base.rb'

class Tempura < Parser
  HOST = "http://eshsushi.ru"
  def self.main
    init
    fl = check_cat_id "eshsushi"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "eshsushi"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div#menu ul li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text.strip)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text.strip}</category>\n" 
      doc2 = Nokogiri::HTML(get HOST+cat["href"])
      subs = doc2.css("div.katalog_line")

      if subs.size > 1
        subs.each do |l|
          subname = l.css("div.razd_name").text.strip
          sub_cat = gen_id(fl,parrent_cat,subname)
          @cats << "<category id='#{sub_cat}' parentId='#{parrent_cat}'>#{subname}</category>\n" 
          l.css("div.kind").each do |tov|
            get_offers tov, sub_cat
          end
        end
      else

        doc2.css("div.katalog_line").each do |l|
          l.css("div.kind").each do |tov|
            puts "!!!!!!!!!!!!!"
            get_offers tov, parrent_cat
          end
        end
      end
    end
   # puts @cats
  gen_xml(HOST,"Ешь-Sushi","eshsushi")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.name").text.strip
      description = tovar.css("div.kind_text > text()").text.strip
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.price").text.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+Nokogiri::HTML(tovar.css("div.kind_img img").first['onmouseover'].scan(/Tip\(\'(.*>)/).join).css("img").first["src"] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main