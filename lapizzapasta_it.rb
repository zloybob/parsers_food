require './base.rb'

class Tempura < Parser
  HOST = "http://lapizzapasta.ru"
  def self.main
    init
  
    %w(it nip).each do |type|
          fl = check_cat_id "lapizzapasta_#{type}"
          @cat_index = fl.values.max if fl
          @fl_o = check_offer_id "lapizzapasta_#{type}"
          @tovar_index = @fl_o.values.max if @fl_o
          html = get(HOST+"/index.php/#{type}")
          doc = Nokogiri::HTML(html)
          doc.css("div.prodlevel_0").each do |cat|
            parrent_cat = gen_id(fl,"0",cat.css("div.prodlevel_0_name h1").text)
            @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.css("div.prodlevel_0_name h1").text}</category>\n" 

            cat.xpath("./div[starts-with(@id,'product_')]").each do |tov|
              get_offers(tov,parrent_cat)
            end
          end
          gen_xml(HOST,"lapizzapasta","lapizzapasta_#{type}")
    end
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.ProductConteyner_name h2").text.strip
      description = tovar.css("div.ProductConteyner_desc").text.strip
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.ProductConteyner_price span.productPrice").text.gsub(".00 RUB","").strip}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{tovar.css("center a").first['href']  rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main