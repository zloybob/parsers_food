require 'curb'
require "nokogiri"
require 'unicode'
require 'digest/md5'
require 'open-uri'
require 'net/http'
require 'json'

class String
  def to_price
    self.scan(/\d+/).join
  end
end

class Parser
 
  private
    class << self
    def gen_id(fl,parrent,name)
      n = Digest::MD5.hexdigest("#{parrent}#{name}")
      parrent_cat = if fl
        fl[n] ? fl[n] : (@cat_index+=1)
      else
        @cat_index+=1
      end
      @md5_cat_str << "#{n};#{parrent_cat}\n"
      return parrent_cat
    end

    def gen_offer_id(fl,name,description)
      n = Digest::MD5.hexdigest(name+description)
      t_i = if @fl_o
        
        @fl_o[n] ? @fl_o[n] : (@tovar_index+=1)
      else
        @tovar_index+=1
      end
      @md5_offer_str << "#{n};#{t_i}\n"
      @md5_offer_hash[n] = t_i
      return t_i
    end

     def init
      @cats = ""
      @offers = ""
      @cat_index = 0
      @tovar_index = 0
      @md5_offer_str = ""
      @md5_offer_hash = {}
      @md5_cat_str = ""
    end

    def check_cat_id(en_name)
      if File.exist?  "#{en_name}_cat.lg"
        id_hash = {}
        File.read("#{en_name}_cat.lg").split("\n").map{|a| md,id = a.split(";");id_hash[md] = id.to_i}
        return id_hash.size > 0 ? id_hash : false
      else
        return false
      end
    end

     def check_offer_id(en_name)
      if File.exist?  "#{en_name}_offer.lg"
        id_hash = {}
        File.read("#{en_name}_offer.lg").split("\n").map{|a| md,id = a.split(";");id_hash[md] = id.to_i}
        return id_hash.size > 0 ? id_hash : false
      else
        return false
      end
    end

    def gen_xml(host,name,en_name)
      xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
      <!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">
        <yml_catalog date=\"#{Time.now.strftime("%Y-%m-%d %H:%M")}\">
          <shop>
            <name>#{name}</name>
            <company>#{name}</company>
            <url>#{host}</url>
            <currencies>
              <currency id=\"RUR\" rate=\"1\"/>
            </currencies>
            <categories>
            #{@cats}
            </categories>
            <offers>
            #{@offers}
            </offers>
            </shop>
  </yml_catalog>"
  File.open("#{en_name}.xml","w+"){|f| f.puts xml}
  File.open("#{en_name}_cat.lg","w+"){|f| f.puts @md5_cat_str}
  File.open("#{en_name}_offer.lg","w+"){|f| f.puts @md5_offer_str}
    end
    def get(url)
      try_count = 0
      begin
        puts "GET: #{url}"
        tmp = Curl::Easy.perform(url) do |curl|
          curl.headers["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.65 Safari/537.36"
          curl.follow_location = true
        # curl.cookiefile = "cookie.txt"
        # curl.cookiejar = "cookie.txt"
        
        end
        content = tmp.body_str
        tmp.close unless tmp.nil?
        # puts content
        content
      rescue Exception => e
        try_count += 1
        sleep 1
        retry if try_count < 10
        tmp.close  unless tmp.nil?
        puts "#{Time.now} [ERROR]: CANT GET PAGE"
        puts e.message
        puts e.backtrace.inspect
        ""
      end
    end
  end

end