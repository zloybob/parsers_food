require './base.rb'

class Tempura < Parser
  HOST = "http://maki-sushi.ru"
  def self.main
    init
    fl = check_cat_id "maki-sushi"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "maki-sushi"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("ul.cat-list li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text.strip)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text.strip}</category>\n" 
      Nokogiri::HTML(get cat["href"]).css("div.product-image a").each do |l|
        get_offers Nokogiri::HTML(get l["href"]), parrent_cat
      end
    end
   # puts @cats
  gen_xml(HOST,"maki-sushi","maki-sushi")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("h1.product-title").text
      description = tovar.css("div.product-details-desc").text
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("span.price").first.text.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{tovar.css("li.product-details-image a").first['href'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main