require './base.rb'

class FoodClick < Parser
  HOST = "http://foodclick.ru"
  def self.main
    init
    html = get(HOST+"/menu.php")
    doc = Nokogiri::HTML(html)
    fl = check_cat_id "foodclick"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "foodclick"
    @tovar_index = @fl_o.values.max if @fl_o
    doc.css("a.menuh3").each do |cat|
      parrent_cat = if fl
        n = Digest::MD5.hexdigest("0"+cat.text.strip)
        fl[n] ? fl[n] : (@cat_index+=1)
      else
        @cat_index+=1
      end
      c = cat['href']

      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      @md5_cat_str << "#{Digest::MD5.hexdigest("0"+cat.text)};#{parrent_cat}\n"
      html = get("#{HOST}/#{c}")
      tovar = Nokogiri::HTML(html)
      get_offers tovar,parrent_cat
      
    end
    gen_xml(HOST,"FoodClick","foodclick")
  end

  def self.get_offers(doc,cat_id)
    doc.css("table.tiptab3").each do |tovar|
      name = tovar.css("a.menueda strong").text
      description = tovar.css("div.menueda2").map{|a| a.text}.join("\n")
      t_i = if @fl_o
        n = Digest::MD5.hexdigest(name+description)
        @fl_o[n] ? @fl_o[n] : (@tovar_index+=1)
      else
        @tovar_index+=1
      end
      @md5_offer_str << "#{Digest::MD5.hexdigest(name+description)};#{t_i}\n"
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("td.price").text.gsub("РУБ","").strip}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+"/"+tovar.css("td a img").first['src']}</picture>
      </offer>\n"
    end
  end

  
end

FoodClick.main