require './base.rb'

class Tempura < Parser
  HOST = "http://shinto.su"
  def self.main
    init
    fl = check_cat_id "shinto"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "shinto"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("ul.menu").first.css("li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text.strip)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text.strip}</category>\n" 
      html = get HOST+cat["href"]
      parts = html.split('<h2 style="padding-left:29px;')
      parts.shift
      if parts.size > 0
        parts.each do |part|
          part = '<h2 style="padding-left:29px;'+part
          doc2 = Nokogiri::HTML(part)
          sub_cat = gen_id(fl,parrent_cat,doc2.css("h2").first.text)
          @cats << "<category id='#{sub_cat}' parentId='#{parrent_cat}'>#{doc2.css("h2").first.text}</category>\n" 
          doc2.css("li a.name").each do |a|
            get_offers Nokogiri::HTML(get HOST+a["href"]), sub_cat
          end
        end
      else
        Nokogiri::HTML(html).css("div.products-list-wrap li a.name").each do |a|
          get_offers Nokogiri::HTML(get HOST+a["href"]), parrent_cat
        end
      end
    end
  gen_xml(HOST,"shinto","shinto")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.content h2").first.text
      description = tovar.css("div.description").text.strip
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.price").text.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("div.img img").first['src'].gsub("/image.php?image=","").gsub(/\&width=\d+/,"").gsub(" ","%20") rescue ""}</picture>
      </offer>\n"
    
  end


  
end

Tempura.main