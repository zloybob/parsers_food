require './base.rb'

class Tempura < Parser
  HOST = "http://пицца-паста54.рф"
  def self.main
    init
    uniq_array = []
    fl = check_cat_id "пицца-паста54"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "пицца-паста54"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div.categories-wrap p.category-capt-txt a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get cat["href"]
      tovar = Nokogiri::HTML(html)
      tovar.css("div.product-item div.wrapper a").each do |a|
        next if uniq_array.include? a["href"]
        uniq_array << a["href"]
        html = get a['href']
        tov = Nokogiri::HTML(html)
        get_offers tov, parrent_cat
      end
    end
    gen_xml(HOST,"Пицца Паста","пицца-паста54")
  end

  def self.get_offers(tovar,cat_id)

      name = tovar.css("h1").text
      description = tovar.css("div.user-inner p").map{|p| p.text}.join("")

      t_i = gen_offer_id @fl_o,name,description
    
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("span.product-price-data").last.text.strip}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("a.fancy-img").first['href'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main