require './base.rb'
class Fsfood < Parser
  HOST = "http://shoko72.ru/"

  def self.main
    init
    fl = check_cat_id "shoko72"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "shoko72"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    #прости меня за говнокод ниже, о бог всех программистов....
    doc.css("ul#left-menu").xpath("./li").each do |cat|
      parrent_id = gen_id(fl,"0",cat.css("a").first.text)
      @cats << "<category id='#{ parrent_id}' parentId='0'>#{cat.css("a").first.text }</category>\n" 
      cat.xpath("./ul").xpath("./li").each do |a|
        n = a.css("a").first.text.strip rescue ""
        next if n == "" || n == "Все блюда"
        sub_cat = a.xpath("./ul").xpath("./li")
        if sub_cat.size > 0
          sub_parrent_id = gen_id(fl,parrent_id,n)
          @cats << "<category id='#{sub_parrent_id}' parentId='#{parrent_id}'>#{n }</category>\n" 
          sub_cat.each do |b|
            sub2_cat = b.xpath("./ul").xpath("./li")
            if sub2_cat.size > 0 
              sub2_parrent_id = gen_id(fl,sub_parrent_id,b.css("a").first.text)
              @cats << "<category id='#{sub2_parrent_id}' parentId='#{sub_parrent_id}'>#{b.css("a").first.text }</category>\n"              
              sub2_cat.each do |c|
                sleep Random.rand(3)
                html = get c.css("a").first['href']
                doc_3 = Nokogiri::HTML(html)
                get_offers(doc_3,sub2_parrent_id)
              end             
            else
              sleep Random.rand(3)
              html = get b.css("a").first['href']
              doc_2 = Nokogiri::HTML(html)
              get_offers(doc_2,sub_parrent_id)
            end
          end
        else
          sleep Random.rand(3)
          html = get a.css("a").first['href']
          doc_1 = Nokogiri::HTML(html)
          get_offers(doc_1,parrent_id)
        end
      end
    end
    gen_xml(HOST,"Шоколадница","shoko72")
  end


  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.product-title").text
      description = tovar.css("td.product-detail-desc > text()").text.gsub("\t","").gsub("\n","")
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.product-price").text.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("div.product-image a").first['href'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Fsfood.main