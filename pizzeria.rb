require './base.rb'

class Tempura < Parser
  HOST = "http://xn--e1afbzh8aa9e.ws/"
  def self.main
    init
    fl = check_cat_id "пиццерия"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "пиццерия"
    @tovar_index = @fl_o.values.max if @fl_o
    #pizza
    html = get("http://xn--e1afbzh8aa9e.ws/index62b9.html")
    cat_name = "Пицца"
    parrent_cat = gen_id(fl,"0",cat_name)
    @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat_name}</category>\n" 
    doc = Nokogiri::HTML(html)
    doc.css("div.item").each do |cat|
    	get_offers cat,parrent_cat
    end
    #sushi
    html = get("http://xn--e1afbzh8aa9e.ws/index62b8.html")
    doc = Nokogiri::HTML(html)
    cat_name = "Пицца"
    parrent_cat = gen_id(fl,"0",cat_name)
    @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat_name}</category>\n" 
    doc.css("div.cat-item a").each do |cat|
      cat_name = cat.text
      sub_cat = gen_id(fl,parrent_cat,cat_name)
      @cats << "<category id='#{sub_cat}' parentId='#{parrent_cat}'>#{cat_name}</category>\n" 
      Nokogiri::HTML(get cat["href"]).css("div.item").each do |cat2|
        get_offers2 cat2,sub_cat
      end
    end
    gen_xml(HOST,"пиццерия","пиццерия")
  end

   def self.get_offers2(tovar,cat_id)
      name = tovar.css("h3.title-position-menu").text
      description = tovar.css("a.image-position-menu img").first["alt"] rescue ""
      img = tovar.css("a.image-position-menu img").first["src"] rescue ""
      price = tovar.css("span.price-position-menu").text.to_price
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{price}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+img}</picture>
      </offer>\n"
  end


  def self.get_offers(tovar,cat_id)
      name = tovar.css("h3.title-position-menu").text
      description = tovar.css("a.image-position-menu img").first["alt"] rescue ""
      img = tovar.css("a.image-position-menu img").first["src"] rescue ""
      tovar.css("div.diam").each do |p|
        sub_name = p.css("span").first.text
        n = name+" "+sub_name
        price = p.css("input").first["data-price"].to_price
        t_i = gen_offer_id @fl_o,n,description
        @offers << "<offer id=\"#{t_i}\">
        <url>#{}</url>
        <price>#{price}</price>
        <currencyId>RUR</currencyId>
        <categoryId>#{cat_id}</categoryId>
        <name>#{n}</name>
        <description>#{description}</description>
        <picture>#{HOST+img}</picture>
        </offer>\n"
      end
    
  end

  
end

Tempura.main