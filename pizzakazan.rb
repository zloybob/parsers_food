require './base.rb'


# 'VALUE':'(\d+)'
class Tempura < Parser
  HOST = "http://pizzakazan.com"
  def self.main
    init
    fl = check_cat_id "pizzakazan"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "pizzakazan"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get("http://pizzakazan.com/catalog/pizza/")
    doc = Nokogiri::HTML(html)
    doc.css("div#menuInner ul li").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.css("a").text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.css("a").text}</category>\n" 


      sleep Random.rand 5
      html = get HOST+cat.css("a").first["href"]
      tovar = Nokogiri::HTML(html)
      sub = tovar.css("ul.menuLeft")
      if sub.size > 0
        sub.css("li a").each do |ss|
          next if ss.text == "Все"
          sub_cat = gen_id(fl,parrent_cat,cat.text)
          @cats << "<category id='#{sub_cat}' parentId='#{parrent_cat}'>#{ss.text}</category>\n" 

        end
      else
        # html2 = get HOST+tovar.css("div.bx_catalog_item_container a").first['href']
        # tov = Nokogiri::HTML(html2)
        # sleep random 3
        # get_offers tov,parrent_cat
      end
      # tovar.css("td.contentBoxContents1 tr.contentBoxContents1 td.contentBoxContents1").each do |tov|
      #   get_offers tov, parrent_cat
      # end
    end
    puts @cats
    # gen_xml(HOST,"Пицца Хаус Казань","pizzakazan")
  end

  def self.get_offers(tovar,cat_id)
      title = tovar.css("b.link_r").text
      name = title
      description = tovar.css("div.link").text.gsub(title,"")
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.cena").first.text.gsub(" руб.","").strip}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{title}</name>
      <description>#{}</description>
      <picture>#{HOST+"/shop/"+tovar.css("a.highslide").first['href'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main