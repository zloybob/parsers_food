require './base.rb'

class Tempura < Parser
  HOST = "http://sushisami.ru"
  def self.main
    init
    fl = check_cat_id "sushisami"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "sushisami"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST+"/catalog")
    doc = Nokogiri::HTML(html)

    doc.css("li.cat-item").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.css("a").first.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.css("a").first.text}</category>\n" 
      html = get cat.css("a").first['href']+"?per_page=100"
      doc2 = Nokogiri::HTML(html)
      sub_cat = doc2.xpath("//li/a[starts-with(@style,'padding-')]")
      if sub_cat.size > 0
        sub_cat.css("a").each do |h|
          sub_parrent_cat = gen_id(fl,parrent_cat,cat.css("a").first.text)
          @cats << "<category id='#{sub_parrent_cat}' parentId='#{parrent_cat}'>#{h.text}</category>\n" 
          html = get h["href"]+"?per_page=100"
          doc3 = Nokogiri::HTML(html)
          doc3.css("div.listing").each do |tov|
            get_offers(tov,sub_parrent_cat)
          end
        end
      else
        doc2.css("div.listing").each do |tov|
          get_offers(tov,parrent_cat)
        end
      end

    end   
    gen_xml(HOST,"Сушисами","sushisami")
  end

  def self.get_offers(tovar,cat_id)
      name = Unicode::capitalize tovar.css("div.contm h3 a").first.text.strip
      description = tovar.css("div.contm p").first.text.strip
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("span.num").text.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{tovar.css("div.imagem img").first['src'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main