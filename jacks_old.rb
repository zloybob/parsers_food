require './base.rb'

class Tempura < Parser
  HOST = "http://jacks.ru"
  def self.main
    init
    fl = check_cat_id "jacks_old"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "jacks_old"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST+"/info_19.htm")
    doc = Nokogiri::HTML(html)
    doc.css("div#hd_menu li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get cat["href"]
      tovar = Nokogiri::HTML(html)
      sub_cat = tovar.css("div.l_main h2 a")
      if sub_cat.size > 0
        sub_cat.each do |sub|
          sub_parrent_cat = gen_id(fl,parrent_cat,sub.text)
          @cats << "<category id='#{sub_parrent_cat}' parentId='#{parrent_cat}'>#{sub.text}</category>\n" 
          html2 = get sub["href"]
          parts = html2.split('<h2 class="m15"')
          if parts.size > 2
            parts.shift
            parts.each do |part|
              part = '<h2 class="m15"'+part
              part = part.force_encoding("windows-1251")
              tov = Nokogiri::HTML(part)
              sub2_name = tov.css("h2.m15").first.text
              @sub2_name = if sub2_name.strip == "" || sub2_name.size < 3
                @sub2_name
              else
                sub2_name
              end
              sub2_parrent_cat = gen_id(fl,sub_parrent_cat,sub2_name)
              @cats << "<category id='#{sub2_parrent_cat}' parentId='#{sub_parrent_cat}'>#{@sub2_name}</category>\n" 
              tov.css("div.product_block").each do |bl|
                get_offers bl, sub2_parrent_cat
              end
            end
          else
            tov = Nokogiri::HTML(html2)
            tov.css("div.product_block").each do |bl|
              get_offers bl, sub_parrent_cat
            end
          end
        end
      else
        parts = html.split('<h2 class="m15"')
        if parts.size > 2
          parts.shift
          parts.each do |part|
            part = '<h2 class="m15"'+part
            part = part.force_encoding("windows-1251")
            tov = Nokogiri::HTML(part)
            sub2_name = tov.css("h2.m15").first.text
            @sub2_name = if sub2_name.strip == "" || sub2_name.size < 3
              @sub2_name
            else
              sub2_name
            end
            sub2_parrent_cat = gen_id(fl,parrent_cat,sub2_name)
            @cats << "<category id='#{sub2_parrent_cat}' parentId='#{parrent_cat}'>#{@sub2_name}</category>\n" 
            tov.css("div.product_block").each do |bl|
              get_offers bl, sub2_parrent_cat
            end
          end
        else
          tovar.css("div.product_block").each do |bl|
            get_offers bl, parrent_cat
          end
        end
      end
    end
   puts @cats
    gen_xml(HOST,"Jack`s","jacks_old")
  end

  def self.get_offers(tovar,cat_id)

    price = tovar.css("div.order_inner b").text
      if price.include? "-"
        html = get tovar.css("h4.agreen a").first['href']
        tov = Nokogiri::HTML(html)

        if tov.css("div.m20 div.m5").size > 0
          tov.css("div.m20 div.m5").each do |item|
            name = tovar.css("h4.agreen a").first.text
            return if name.include?("Составная Пицца") || name.include?("Сладкая парочка")
            price = item.css("label").text.scan(/\d+/).flatten.last
            name2 = item.css("label").text.split("\n").first
            name_f = name +" "+ name2.strip.gsub(name,"")
            puts name_f
            puts price
            description = tovar.css("div#main div.m10").text rescue ""
            t_i = gen_offer_id @fl_o,name_f,description
            @offers << "<offer id=\"#{t_i}\">
            <url>#{}</url>
            <price>#{price.scan(/\d+/).join}</price>
            <currencyId>RUR</currencyId>
            <categoryId>#{cat_id}</categoryId>
            <name>#{name_f}</name>
            <description>#{description}</description>
            <picture>#{HOST+"/shop/"+tovar.css("a.highslide").first['href'] rescue ""}</picture>
            </offer>\n"
          end
        else
          tov.css("div.s14").each do |item|
            name = tovar.css("h4.agreen a").first.text
            return if name.include?("Составная Пицца") || name.include?("Сладкая парочка")
            price = item.text.scan(/\d+/).flatten.last
            name2 = item.text.split("\n").first
            name_f = name+" " + name2.strip.gsub(name,"")
            puts price
            description = tovar.css("div#main div.m10").text rescue ""
            t_i = gen_offer_id @fl_o,name_f,description
            @offers << "<offer id=\"#{t_i}\">
            <url>#{}</url>
            <price>#{price.scan(/\d+/).join}</price>
            <currencyId>RUR</currencyId>
            <categoryId>#{cat_id}</categoryId>
            <name>#{name_f}</name>
            <description>#{description}</description>
            <picture>#{HOST+"/shop/"+tovar.css("a.highslide").first['href'] rescue ""}</picture>
            </offer>\n"
          end
        end
      else
        name = tovar.css("h4.agreen a").first.text
        description = tovar.css("div.s11").text rescue ""
        t_i = gen_offer_id @fl_o,name,description
        @offers << "<offer id=\"#{t_i}\">
        <url>#{}</url>
        <price>#{price.scan(/\d+/).join}</price>
        <currencyId>RUR</currencyId>
        <categoryId>#{cat_id}</categoryId>
        <name>#{name}</name>
        <description>#{description}</description>
        <picture>#{HOST+"/shop/"+tovar.css("a.highslide").first['href'] rescue ""}</picture>
        </offer>\n"
      end
    
  end

  
end

Tempura.main