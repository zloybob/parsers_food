require './base.rb'

class Hayati < Parser
  HOST = "http://moskvasushi.ru"
  def self.main
    init
    fl = check_cat_id "moskvasushi"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "moskvasushi"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div.block-content ul").first.xpath('./li').each do |cat|
      sub_cat =cat.xpath("./ul").xpath('./li')
      parrent_cat = gen_id(fl,"0",sub_cat.size > 0 ? (Unicode::capitalize(cat.css("span a").first.text.strip)) : (Unicode::capitalize(cat.css("span").text.strip)))
      
      if sub_cat.size > 0
        @cats << "<category id='#{parrent_cat}' parentId='0'>#{Unicode::capitalize  cat.css("span a").first.text.strip}</category>\n" 
        html = get cat.css("a").first['href']+"?per_page=100"
        tovar = Nokogiri::HTML(html)
        tovar.css("div.menu-item-box.item-box").each do |tov|
          get_offers tov, parrent_cat
        end
        sub_cat.each do |sub|
          child_cat = gen_id(fl,parrent_cat,Unicode::capitalize(sub.css("span a").text.strip))
          @cats << "<category id='#{child_cat}' parentId='#{parrent_cat}'>#{Unicode::capitalize  sub.css("span a").text.strip}</category>\n"
          html = get sub.css("a").first['href']+"?per_page=100"
          tovar = Nokogiri::HTML(html)
          tovar.css("div.menu-item-box.item-box").each do |tov|
            get_offers tov, child_cat
          end
        end
      else
        @cats << "<category id='#{parrent_cat}' parentId='0'>#{Unicode::capitalize  cat.css("span").text.strip}</category>\n" 
        html = get cat.css("a").first['href']+"?per_page=100"
        tovar = Nokogiri::HTML(html)
        tovar.css("div.menu-item-box.item-box").each do |tov|
          get_offers tov, parrent_cat
          end
      end
    end
   
    gen_xml(HOST,"Москва Суши","moskvasushi")
  end

  def self.get_offers(tovar,cat_id)
      name = Unicode::capitalize tovar.css("div.title h4 a").text
      description = tovar.css("div.menu-item-info").text
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("li.minPrice span.num").text}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{tovar.css("img").first["src"] rescue ""}</picture>
      </offer>\n"
     
      
    
  end

  
end

Hayati.main