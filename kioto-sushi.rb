require './base.rb'

class Kioto < Parser
  HOST = "http://kioto-sushi.ru"
  def self.main
    init
    fl = check_cat_id "kioto-sushi"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "kioto-sushi"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div.catalog-buttons a").each_with_index do |cat,i|
      next if i == 0
      parrent_cat = gen_id(fl,"0",cat.text.gsub(/\(\d+\)/,'').strip.gsub(" ",""))
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text.gsub(/\(\d+\)/,'').strip.gsub(" ","")}</category>\n" 
      html = get HOST+cat["href"]
      tovar = Nokogiri::HTML(html.encode("UTF-8","cp1251"))
      tovar.css("table.catalog-elements table.catalog-element").each do |tov|
        get_offers tov, parrent_cat
      end
    end
    gen_xml(HOST,"Киото суши","kioto-sushi")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("h3.name").text
      description = tovar.css("p.descrip").text.strip.gsub(/\t/,"").gsub("\n","")
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("p.price").text}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("td.photo").first['style'].gsub(/background-image:url\((.*)\);/,"\\1") rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Kioto.main