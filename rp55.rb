require './base.rb'

class Tempura < Parser
  HOST = "http://rp55.ru"
  def self.main
    init
    fl = check_cat_id "rp55"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "rp55"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("section.uk-grid div.uk-panel").each_with_index do |cat,i|
      next if i == 0
    	cat_name = cat.css("div.cattitle").text.strip.gsub(/\s+/,"")
      next if cat_name == ""
      parrent_cat = gen_id(fl,"0",cat_name)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat_name}</category>\n" 
      cat.xpath("descendant::div[starts-with(@class,'catblock')]").each do |tov|
        get_offers tov, parrent_cat
      end
    end
    gen_xml(HOST,"rp55","rp55")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.catname").text
      description = tovar.css("div.sostav").text
      img = tovar.css("div.catimg a").first["href"] rescue ""
      s = tovar.css("div.razm_ves_price")
      s.each do |p|
        price = p.css("span.priceforcalc").text.to_price
        sub_name = p.css("span.rzves").text
        n= s.size > 1 ? name+' '+sub_name : name
        t_i = gen_offer_id @fl_o,n,description
        @offers << "<offer id=\"#{t_i}\">
        <url>#{}</url>
        <price>#{price}</price>
        <currencyId>RUR</currencyId>
        <categoryId>#{cat_id}</categoryId>
        <name>#{n}</name>
        <description>#{description}</description>
        <picture>#{img}</picture>
        </offer>\n"
      end
    
  end

  
end

Tempura.main