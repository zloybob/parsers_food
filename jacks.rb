require './base.rb'

class Tempura < Parser
  HOST = "http://new.jacks.ru"
  def self.main
    init
    fl = check_cat_id "jacks"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "jacks"
    @tovar_index = @fl_o.values.max if @fl_o
    Nokogiri::HTML(get(HOST+"/sections/")).css("div.categories-list a").each do |cat|
      cat_name = cat.css("span span").text
      parrent_cat = gen_id(fl,"0",cat_name)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat_name}</category>\n" 
      Nokogiri::HTML(get HOST+cat["href"]).css("ul#yw0 li a").each do |c|
        cat_name = c.text
        sub_parrent_cat = gen_id(fl,parrent_cat,cat_name)
        @cats << "<category id='#{sub_parrent_cat}' parentId='#{parrent_cat}'>#{cat_name}</category>\n" 
        doc = Nokogiri::HTML(get HOST+c['href'])
        doc.css("div.product").each do |tov|
          get_offers Nokogiri::HTML(get HOST+tov.css("div.product-name a").first['href']), sub_parrent_cat
          # break
        end
        cur_page = doc.css("div.pager ul li.next a").first['href'] rescue nil
        while cur_page
          Nokogiri::HTML(get HOST+cur_page).css("div.product").each do |tov|
            get_offers Nokogiri::HTML(get HOST+tov.css("div.product-name a").first['href']), sub_parrent_cat
            # break
          end
          cur_next_page = doc.css("div.pager ul li.next a").first['href'] rescue nil
          cur_page = unless cur_next_page || cur_next_page == cur_page
            cur_next_page
          else
            nil
          end
        end
      end
      # break
    end
    # puts @cats
    @cats.gsub!("&","&amp;")
    gen_xml(HOST,"Jack`s","jacks")
  end

  def self.get_offers(tovar,cat_id)
    description = tovar.css("div.dynamic-description").text.strip.gsub("\n"," ").gsub("&laquo",'"').gsub("&raquo;",'"').gsub("&","&amp;")
    name = tovar.css("div.product-main-info h1").first.text.gsub("&laquo",'"').gsub("&raquo;",'"').gsub("&","&amp;")
    price = tovar.css("span.dynamic-price").first.text.to_price
      if tovar.css("select.product-option").size == 1
        tovar.css("select.product-option option").each do |item|
          item_price = item['data-price'].to_i == 0 ? price : item['data-price']
          item_name = item.text
          tname = (name+ " " +item_name).gsub("&laquo",'"').gsub("&raquo;",'"')
           t_i = gen_offer_id @fl_o,name,description
          @offers << "<offer id=\"#{t_i}\">
          <url>#{}</url>
          <price>#{item_price}</price>
          <currencyId>RUR</currencyId>
          <categoryId>#{cat_id}</categoryId>
          <name>#{tname}</name>
          <description>#{description}</description>
          <picture>#{HOST+(item['data-image'].to_s == "" ? tovar.css("div.product-pic a").first['href'] : item['data-image']) rescue ""}</picture>
          </offer>\n"
        end
      else
        t_i = gen_offer_id @fl_o,name,description
        @offers << "<offer id=\"#{t_i}\">
        <url>#{}</url>
        <price>#{price}</price>
        <currencyId>RUR</currencyId>
        <categoryId>#{cat_id}</categoryId>
        <name>#{name}</name>
        <description>#{description}</description>
        <picture>#{HOST+tovar.css("div.product-pic a").first['href'] rescue ""}</picture>
        </offer>\n"
      end
      
    
  end

  
end

Tempura.main
