require './base.rb'
 
class Tempura < Parser
  HOST = "http://gedza.ru"
  def self.main
    init
    @offer_hash = {}
    fl = check_cat_id "gedza"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "gedza"
    @tovar_index = @fl_o.values.max if @fl_o
    #япония
    parrent_cat = gen_id(fl,"0","GEDZA")
    @cats << "<category id='#{parrent_cat}' parentId='0'>GEDZA</category>\n" 
    html = get("http://gedza.ru/menu/95/")
    doc = Nokogiri::HTML(html)
    doc.css("div.menu-block-container a").each do |cat|
      next unless cat["href"] =~ /^\/menu\//
      sub_cat = gen_id(fl,parrent_cat,cat.text) 
      @cats << "<category id='#{sub_cat}' parentId='#{parrent_cat}'>#{cat.text}</category>\n" 
      Nokogiri::HTML(get HOST+cat["href"]).css("ul.menu-dish-list li.menu-dish-list-item").each do |t|
        get_offers t, sub_cat
      end
    end
    #италия
    parrent_cat = gen_id(fl,"0","Primasole")
    @cats << "<category id='#{parrent_cat}' parentId='0'>Primasole</category>\n" 
    html = get("http://gedza.ru/menu/172/")
    doc = Nokogiri::HTML(html)
    doc.css("div.menu-block-container a").each do |cat|
      next unless cat["href"] =~ /^\/menu\//
      sub_cat = gen_id(fl,parrent_cat,cat.text) 
      @cats << "<category id='#{sub_cat}' parentId='#{parrent_cat}'>#{cat.text}</category>\n" 
      Nokogiri::HTML(get HOST+cat["href"]).css("ul.menu-dish-list li.menu-dish-list-item").each do |t|
        get_offers t, sub_cat
      end
    end
    @offer_hash.each do |k,v|
      gen_offer v
    end
    @cats.gsub!("&","&amp;")
    @offers.gsub!("&","&amp;")
    gen_xml(HOST,"gedza","gedza")
  end

  def self.gen_offer(h)
    @offers << "<offer id=\"#{h['index']}\">
        <url>#{}</url>
        <price>#{h['price']}</price>
        <currencyId>RUR</currencyId>
        #{h['cat_id'].map{|c| "<categoryId>#{c}</categoryId>"}.join("\n")}
        <name>#{h['name']}</name>
        <description>#{h['description']}</description>
        <picture>#{h['img']}</picture>
        </offer>\n"
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("a.menu-dish-list-item-name").text.strip
      description = tovar.css("input.dish-text").first["value"]
      r=Digest::MD5.hexdigest(name+description)

      unless @offer_hash[r]
        @offer_hash[r] ||= {}
        @offer_hash[r]["name"] = name
        @offer_hash[r]["description"] = description
        @offer_hash[r]["index"] = gen_offer_id @fl_o,name,description
        @offer_hash[r]["img"] = HOST+(tovar.css("img").first['src'] rescue "")
        @offer_hash[r]["price"] = tovar.css("div.menu-dish-list-item-price").text.to_price
        @offer_hash[r]["cat_id"] ||= []
        @offer_hash[r]["cat_id"] << cat_id
      else
        @offer_hash[r]["cat_id"] << cat_id
      end
    
  end

  
end

Tempura.main