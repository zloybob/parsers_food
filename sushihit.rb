require './base.rb'

class Tempura < Parser
  HOST = "http://sushihit.ru"
  def self.main
    init
    fl = check_cat_id "sushihit"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "sushihit"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div.menu_level1 a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get HOST+cat["href"]
      tovar = Nokogiri::HTML(html)
      
      tovar.xpath("//td[starts-with(@valign,'top')]").css("div.cell").each do |tov|
        get_offers tov,parrent_cat
        
      end
      
    end
    puts @cats
    gen_xml(HOST,"SushiHit","sushihit")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("table").first.text
      description = tovar.css("div.cell_pic img").first['title']
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.cell_buy td").first.text.gsub(" руб.","").strip}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("div.cell_pic img").first['src'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main