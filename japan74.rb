require './base.rb'

class Tempura < Parser
  HOST = "http://japan74.ru"
  def self.main
    init
    fl = check_cat_id "japan74"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "japan74"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("ul#category li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      Nokogiri::HTML(get(HOST+cat['href'])).css("div#product div.item").each do |tov|
        get_offers tov, parrent_cat
      end
    end
   # puts @cats
    gen_xml(HOST,"Япония Cуши","japan74")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.title").first.text.strip
      description = tovar.css("div.description").text.strip
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.price").first.text.split(",").first.to_price.to_price}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{HOST+tovar.css("div div img").first['src'] rescue ""}</picture>
      </offer>\n"  
  end
end

Tempura.main