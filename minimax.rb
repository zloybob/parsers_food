require './base.rb'

class Minimax < Parser
  HOST = "http://minimax-pizza.ru"
  def self.main
    init
    fl = check_cat_id "minimax-pizza"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "minimax-pizza"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
   
    doc.css("main.content div.catalog-section").each do |catalog|
      c = catalog.css("div.section-title a").first
      parrent_cat = gen_id(fl,"0",c.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{c.text}</category>\n" 
      
      cat_html = get(HOST+c['href'])
      cat_doc = Nokogiri::HTML(cat_html)
      sub_cats = cat_doc.css("div.catalog-section-list")
      if sub_cats.size > 0
        sub_cats.css("a").each do |a|
          cat_id = gen_id(fl,parrent_cat,a.text)
          @cats << "<category id='#{cat_id}' parentId='#{parrent_cat}'>#{a.text}</category>\n"  
          get_offers(Nokogiri::HTML(get HOST+a['href']))
        end
      else
        get_offers cat_doc
      end
    end
    gen_xml(HOST,"MiniMax Pizza","minimax-pizza")
  end

  def self.get_offers(doc)
    doc.css("div.catalog-item").each do |tovar|   
      tovar.css("div.catalog-item-size div").each do |tov|
        name = tovar.css("div.section-title").text
        description = tovar.css("a.colorbox").first['description'].gsub("<br />","").strip #{tov.css("span.size-wrapper").map{|a| a.css('span')}.join
        t_i = gen_offer_id @fl_o,name,description
        @offers << "<offer id=\"#{t_i}\">
        <url>#{}</url>
        <price>#{tov.css("a").text.strip}</price>
        <currencyId>RUR</currencyId>
        <categoryId>#{@cat_index}</categoryId>
        <name>#{name}</name>
        <description>#{description}</description>
        <picture>#{HOST+tovar.css("a.colorbox").first['href']}</picture>
        </offer>\n"
      end
    end
  end

  
end

Minimax.main