require './base.rb'

class Tempura < Parser
  HOST = "http://paradise-pizza.ru"
  def self.main
    init
    fl = check_cat_id "paradise-pizza"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "paradise-pizza"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("div.catalog-section div.catalog-section").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text.strip)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text.strip}</category>\n" 
      html = get HOST+cat["href"]
      tovar = Nokogiri::HTML(html)
      tovar.css("div.block_product").each do |tov|
        t = Nokogiri::HTML(get(HOST+tov.css("div.image a").first["href"]))
        get_offers t, parrent_cat
      end
    end
    # puts @cats
    gen_xml(HOST,"paradise-pizza","paradise-pizza")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("h1").text.gsub("№","#")
      description = tovar.css("div.short-description").text.strip.gsub(/\s+/," ")
      image = tovar.css("img.product-main-image").first['src'] rescue ""
      puts image
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("span.pricenumber").text.to_price}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{image}</picture>
      </offer>\n"
    
  end

  
end

Tempura.main