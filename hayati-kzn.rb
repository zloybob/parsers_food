require './base.rb'

class Hayati < Parser
  HOST = "http://hayati-kzn.ru"
  def self.main
    init
    fl = check_cat_id "hayati-kzn"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "hayati-kzn"
    @tovar_index = @fl_o.values.max if @fl_o
    parrent_cat = 0
    html = get(HOST)
    doc = Nokogiri::HTML(html)
    doc.css("ul.cat-list ul.cat-list").xpath('./li').each do |cat|
      
      sub_cat =cat.xpath("./ul").xpath('./li')
      if sub_cat.size > 0
        parrent_cat = gen_id(fl,"0",cat.css("span").first.text.strip)
        @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.css("span").first.text.strip}</category>\n" 
        sub_cat.each do |sub|
          
          sub2_cat =sub.xpath("./ul").xpath('./li')
          if sub2_cat.size > 0
            child_cat = gen_id(fl,parrent_cat,sub.css("span").first.text.strip)
            @cats << "<category id='#{child_cat}' parentId='#{parrent_cat}'>#{sub.css("span").first.text.strip}</category>\n"
            html = get sub.css("a").first['href']
            tovar = Nokogiri::HTML(html)
            get_offers tovar, child_cat
            sub2_cat.each do |sub2|
              child2_cat = gen_id(fl,child_cat,sub2_cat.css("span").text.strip)
              @cats << "<category id='#{child2_cat}' parentId='#{child_cat}'>#{sub2_cat.css("span").text.strip}</category>\n"
            end
          else
            child_cat = gen_id(fl,parrent_cat,sub.css("span").text.strip)
             @cats << "<category id='#{child_cat}' parentId='#{parrent_cat}'>#{sub.css("span").text.strip}</category>\n"
              html = get sub.css("a").first['href']
              tovar = Nokogiri::HTML(html)
              get_offers tovar, child_cat
          end
        end
      else
        parrent_cat = gen_id(fl,"0",cat.css("span").text.strip)
        @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.css("span").text.strip}</category>\n" 
        html = get cat.css("a").first['href']
        tovar = Nokogiri::HTML(html)
        get_offers tovar, parrent_cat
      end
    end
   

    gen_xml(HOST,"ХАЯТИ","hayati-kzn")
  end

  def self.get_offers(tovar,cat_id)
    
      tovar.css("div.product-name a").each do |tov2|


        html = get tov2['href']
        tov = Nokogiri::HTML(html)
        name = tov.css("h1.product-title").text
        description = tov.css("div.product-details-desc p").text

        t_i = gen_offer_id @fl_o,name,description
        @offers << "<offer id=\"#{t_i}\">
        <url>#{}</url>
        <price>#{tov.css("span.price").text.gsub(" руб.","")}</price>
        <currencyId>RUR</currencyId>
        <categoryId>#{cat_id}</categoryId>
        <name>#{name}</name>
        <description>#{description}</description>
        <picture>#{tov.css("a.zoom.fancy-modal").first["href"] rescue ""}</picture>
        </offer>\n"
      end
     
      
    
  end

  
end

Hayati.main