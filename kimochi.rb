require './base.rb'

class Kimochi < Parser
  HOST = "http://kimochi-da.ru/"
  def self.main
    init
    fl = check_cat_id "kimochi-da"
    @cat_index = fl.values.max if fl
    @fl_o = check_offer_id "kimochi-da"
    @tovar_index = @fl_o.values.max if @fl_o
    html = get(HOST)
    doc = Nokogiri::HTML(html.encode("UTF-8","UTF-8"))
    doc.css("div.box-category ul li a").each do |cat|
      parrent_cat = gen_id(fl,"0",cat.text)
      @cats << "<category id='#{parrent_cat}' parentId='0'>#{cat.text}</category>\n" 
      html = get cat["href"]
      ar = html.split("<div class=\"image\"><a")
      ar = ar.map{|a| "<div class=\"image\"><a"+a}
      ar.shift
      ar.each do |tov|
      	tovar = Nokogiri::HTML(tov)
      	get_offers tovar,parrent_cat
      end
    end
     gen_xml(HOST,"Kimochi-Da","kimochi-da")
  end

  def self.get_offers(tovar,cat_id)
      name = tovar.css("div.name a").text
      description = tovar.css("div.description").text.strip.gsub("\n","")
      t_i = gen_offer_id @fl_o,name,description
      @offers << "<offer id=\"#{t_i}\">
      <url>#{}</url>
      <price>#{tovar.css("div.price").text.gsub(" р.","").strip.scan(/\d+/).join}</price>
      <currencyId>RUR</currencyId>
      <categoryId>#{cat_id}</categoryId>
      <name>#{name}</name>
      <description>#{description}</description>
      <picture>#{tovar.css("div.image a img").first['src'] rescue ""}</picture>
      </offer>\n"
    
  end

  
end

Kimochi.main